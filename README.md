CubiX
=====

Cubix é um algoritmo de criptografia baseado no cubo de Rubik, onde transforma arquivos em uma representação de cubo utilizando matrizes. Se encontra em desenvolvimento, não estavel.

###Modo de compilação:
Para compilar:<br>
`make all`

Para remover os arquivos compilados:<br>
`make clean`

###Modo de uso:
`./main -c arquivo_de_texto_puro`<br>
`./main -d arquivo_de_texto_criptografado`
